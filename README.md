# Objectif

Développer un utilitaire permettant de générer la liste des éléments JIRA ayant des relations
entre elle. Afin de facilité la prise de décision, le statut, la durée restante à l'histoire, 
le sprint visé par l'histoire, ainsi que la personne assigné y est indiqué. Un code de couleur 
est associé aux différents éléments JIRA selon leur statut.


# Instruction 

- Utiliser la commande "Yarn" pour installer les dépendances
- Mettre le fichier du résultat de la requête JIRA (XML) à la racine sous le nom "request.xml".
- Utiliser la commande "Yarn generate" pour générer un PDF à partir du contenu du fichier.
- Utiliser la commande "Yarn generatePDFFromInputMMD" pour générer un PDF à partir du contenu du fichier.
- Utiliser la commande "Yarn generatePNGFromInputMMD" pour générer un PNG à partir du contenu du fichier.

# Remarques

- Les commandes servant à générer les fichiers visueles génères du code "mermaid" dans un fichier nonmmé "input.mmd". Ce code est un code utilisable avec l'API Mermaid pour créer des diagrammes servants à représenter des relations. (ex https://mermaidjs.github.io/mermaid-live-editor/#/edit/)

- Dans le cas de cette application, nous utilisons le CLI Mermaid qui est intégré à même l'exécutable node. Par conséquent, ce programme n'a pas de dépendance vis-à-vis la version Web de API Mermaid.

- Pour les curieux, il est possible de voir le fichier intermédiare généré "input.mmd". Ce dernier peut être copier/coller dans l'éditeur en ligne du site Mermaid.

- Si vous souhaitez débuger, mettre l'outil "auto-attachement" pour les processus node active au niveau 
de VS Code. La bibliothèque est utilisée pour convertir le XML provenant de JIRA en du JSON beaucoup
plus facilement manipulable pour les fins de cette application. Le fichier "settings.json" de ".vscode" contient déjà cette configuration.


# node-typescript-boilerplate (INSTRUCTIONS QUI VENAIT AVEC LE BOILEPLATE)

A Node.js project boilerplate with Typescript support and auto-reload.
Feel free to customize as you need it.

Use `npm run dev` to start a nodemon watcher over `src` files.

Use `npm start` to run your project without nodemon.

This boilerplate uses: 
* [typescript cli](https://www.npmjs.com/package/typescript) For TS files transpilation.
* [nodemon](https://nodemon.io/) For auto-reload project on changes.
* [ts-node](https://github.com/TypeStrong/ts-node) For execution of TS files.