import { parseString } from "xml2js";
import * as fs from "fs";
import * as _ from "loadsh";


class Hash {
    [key: string]: string
}

class HashTyped<T> {
    [key: string]: T;
}

class HashArray {
    [key: string]: string[]
}

class Main {

    findIndexFieldSprint(jiraElement: any): number {
        for (let i = 0; i < jiraElement.customfields[0].customfield.length; i++) {
            if (jiraElement.customfields[0].customfield[i].customfieldname[0] === "Sprint") {
                return i;
            }
        }
        return -1;
    }

    rapport1() {
        for (let domaine in this.hashJiraElementKeysPerDomain) {
            let jiraElementKeys = this.hashJiraElementKeysPerDomain[domaine]
            for (let jiraElementIndex in jiraElementKeys) {
                let jiraElementKey = jiraElementKeys[jiraElementIndex];
                let jiraElement = this.hashJiraElementParJiraElementKey[jiraElementKey];
                if (jiraElement.issuelinks) {
                    jiraElement.issuelinks.map((issueLink) => {
                        if (issueLink.issuelinktype[0].inwardlinks) {
                            let linkElementKey = issueLink.issuelinktype[0].inwardlinks[0].issuelink[0].issuekey[0]._;
                            let description = issueLink.issuelinktype[0].inwardlinks[0].$.description;

                            if (this.displayKeyOnly) {
                                console.log(jiraElementKey + " --> |" + description + "|" + linkElementKey);
                            } else {
                                console.log(this.hashCaseContentPerKey[jiraElementKey] + " --> |" + description + "|" + this.hashCaseContentPerKey[linkElementKey]);
                            }
                        }
                        console.log("");
                    });
                }
            }
        }
        for (let domaine in this.hashJiraElementKeysPerDomain) {
            console.log("subgraph " + domaine);
            let jiraElementKeys = this.hashJiraElementKeysPerDomain[domaine]
            for (let jiraElementIndex in jiraElementKeys) {
                let jiraElementKey = jiraElementKeys[jiraElementIndex];
                if (this.displayKeyOnly) {
                    console.log(jiraElementKey);
                } else {
                    console.log(this.hashCaseContentPerKey[jiraElementKey]);
                }
                console.log(`click ${jiraElementKey} "${this.hashLinkPerKey[jiraElementKey]}"`);
            }
            console.log("end");
        }
    }

    private hashCaseContentPerKey: Hash = {};
    private hashLinkPerKey: Hash = {};
    private hashJiraElementParJiraElementKey: HashTyped<any> = {};
    private hashJiraElementKeysPerDomain: HashArray = {};
    private hashDomainPerJiraElementKey: Hash = {};
    private displayKeyOnly = false;

    execute(): void {
        const me = this;
        fs.readFile(__dirname + '/../request.xml', (err, data) => {
            parseString(data, function (err, result) {
                console.log("graph LR");

                let classDefs: string[] = [
                    "classDef carnetDeProduit fill:#B3E5FC,stroke:#7D0000,stroke-width:6px;",
                    "classDef aFaire fill:#B3E5FC,stroke:#7D0000,stroke-width:6px;",
                    "classDef enSuspens fill:#FF7043,stroke:#7D0000,stroke-width:6px;",
                    "classDef enCours fill:#69F0AE,stroke:#7D0000,stroke-width:6px;",
                    "classDef enRevue fill:#FFCC80,stroke:#7D0000,stroke-width:6px;",
                    "classDef FerméObjectifSprintA fill:#00E676,stroke:#000,stroke-width:6px;",
                    "classDef carnetDeProduitObjectifSprintA fill:#B3E5FC,stroke:#000,stroke-width:6px;",
                    "classDef aFaireObjectifSprintA fill:#B3E5FC,stroke:#000,stroke-width:6px;",
                    "classDef enSuspensObjectifSprintA fill:#FF7043,stroke:#000,stroke-width:6px;",
                    "classDef enCoursObjectifSprintA fill:#69F0AE,stroke:#000,stroke-width:6px;",
                    "classDef enRevueObjectifSprintA fill:#FFCC80,stroke:#000,stroke-width:6px;",
                    "classDef FerméObjectifSprintA fill:#00E676,stroke:#000,stroke-width:6px;",
                    "classDef carnetDeProduitObjectifSprintB fill:#B3E5FC,stroke:#999,stroke-width:6px;stroke-dasharray: 10, 10",
                    "classDef aFaireObjectifSprintB fill:#B3E5FC,stroke:#999,stroke-width:6px;stroke-dasharray: 10, 10",
                    "classDef enSuspensObjectifSprintB fill:#FF7043,stroke:#999,stroke-width:6px;stroke-dasharray: 10, 10",
                    "classDef enCoursObjectifSprintB fill:#69F0AE,stroke:#999,stroke-width:6px;stroke-dasharray: 10, 10",
                    "classDef enRevueObjectifSprintB fill:#FFCC80,stroke:#999,stroke-width:6px;stroke-dasharray: 10, 10",
                    "classDef FerméObjectifSprintB fill:#00E676,stroke:#999,stroke-width:6px,stroke-dasharray: 10, 10;",
                ];
                let classForJiraElement: string[] = [];
                result.rss.channel[0].item.map((jiraElement) => {
                    let jiraElementKey = jiraElement.key[0]._;
                    let jiraElementTitle = jiraElement.title[0].replace(/"/g, "'");
                    let jiraElementLink = jiraElement.link[0].replace(/"/g, "'");
                    jiraElementTitle = jiraElementTitle.replace(/« /g, "\''");
                    jiraElementTitle = jiraElementTitle.replace(/ »/g, "\''");
                    jiraElementTitle = `<b>${jiraElementTitle}</b><p></p>`;
                    let content = jiraElementTitle; // let content = jiraElementTitle; // `<a href="${jiraElementLink}">${jiraElementTitle}</a>`;

                    let sprint;
                    let contentSprint = content
                    if (jiraElement.customfields) {
                        let indexFieldSprint = me.findIndexFieldSprint(jiraElement);
                        if (indexFieldSprint > -1) {
                            sprint = jiraElement.customfields[0].customfield[indexFieldSprint].customfieldvalues[0].customfieldvalue[0]._;
                            contentSprint = "<p></p>Sprint : " + sprint
                        } else {
                            contentSprint = "<p></p>Sprint : Non défini";
                        }
                    }

                    if (jiraElement.status) {
                        let status = jiraElement.status[0]._;
                        let sprintTrim = ""
                        if (sprint) {
                            sprintTrim = sprint.replace(/\s/g, '')
                            if (sprintTrim !== "ObjectifSprintA" && sprintTrim !== "ObjectifSprintB") {
                                sprintTrim = "";
                            }
                        }

                        switch (status) {
                            case "Carnet de produit":
                                classForJiraElement.push("class " + jiraElementKey + " carnetDeProduit" + sprintTrim);
                                break;
                            case "À faire":
                                classForJiraElement.push("class " + jiraElementKey + " aFaire" + sprintTrim);
                                break;
                            case "En suspens":
                                classForJiraElement.push("class " + jiraElementKey + " enSuspens" + sprintTrim);
                                break;
                            case "En cours":
                                classForJiraElement.push("class " + jiraElementKey + " enCours" + sprintTrim);
                                break;
                            case "En revue":
                                classForJiraElement.push("class " + jiraElementKey + " enRevue" + sprintTrim);
                                break;
                            case "Fermé":
                                classForJiraElement.push("class " + jiraElementKey + " Fermé" + sprintTrim);
                                break;
                            default:
                                break;
                        }
                        content = content + "<p></p>Statut : " + status;
                    }
                    if (jiraElement.timeestimate) {
                        content = content + "<p></p>Travail restant estimé : " + jiraElement.timeestimate[0]._;
                    } else {
                        content = content + "<p></p>Travail restant estimé : Non estimé";
                    }
                    content = content + contentSprint;
                    if (jiraElement.assignee) {
                        content = content + "<p></p>Assigné à : " + jiraElement.assignee[0]._;
                    }
                    me.hashCaseContentPerKey[jiraElementKey] = jiraElementKey + '["' + content + '"]'
                    me.hashLinkPerKey[jiraElementKey] = jiraElementLink;

                    let domaine = jiraElementTitle;
                    domaine = domaine.substr(jiraElementTitle.indexOf("]") + 1);
                    let positionDelimiter = domaine.indexOf("-");
                    if (positionDelimiter <= 0 || positionDelimiter > 20) {
                        domaine = "Divers"
                    } else {
                        domaine = domaine.substr(0, positionDelimiter).trim();
                    }
                    if (me.hashJiraElementKeysPerDomain[domaine] == undefined) {
                        me.hashJiraElementKeysPerDomain[domaine] = [];
                    }
                    me.hashJiraElementKeysPerDomain[domaine].push(jiraElementKey);
                    me.hashDomainPerJiraElementKey[jiraElementKey] = domaine;
                    me.hashJiraElementParJiraElementKey[jiraElementKey] = jiraElement;
                });

                me.rapport1();

                classDefs.map(element => {
                    console.log(element);
                });
                classForJiraElement.map(element => {
                    console.log(element);
                });

            });
        });
    }
}

const program: Main = new Main();
program.execute();